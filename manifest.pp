$svc_conf_template = @(END)

[Unit]
Description=ConfigServer para micro serviços de Cliente, Cartao e Pagamentos

[Service]
ExecStart=/usr/bin/java -jar /home/ubuntu/api-investimentos.jar

[Install]
WantedBy=multi.user.target

END


$authorized_keys = @(END)

ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDpkVURrihaBSBor76bPsqQnwdgYG1FradT3K+hFJieHv3171uCk+YssdOTYQ8SpycbN6yERwwsR2VqYD98eiLdyNAfRpikSB7W5sRdkrbqSGmnvHvbXrp0y0BDhbCblvblzrm2aq71nTFTcYHsrxsyIZEkWhM3BLAO9HdVKR0FPdXvfrkjzrYhOZmcMFn3fJsbql4yQK/AV025whBrQ05sM5z2pUyd0wlOcIYEPyydbb+5BqoJCPOjaZnEOYOOklpi2iH/1wNzo0IJzlpU4TP6n165RwAhUwsBQ5EutmGNehI61S2BJ5Q8ipE4BcS4rPg5jbKuST/LWuKqNbBi5Cf5 diego-rosendo--keypair
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDAkWA9L5XoTi13RsiOEllOZ6DeuMAmEM+UniMLK9Rx7ZzUw4NA8XhxBY56v2CGMbJGLFGT5gZUh31iuTh8o3mQr99k+q+Uf5jj2qLgq9iYkFUqvMrt/c5Hd/BnPz3K2rSXe7JouMK4BiwzKGoGctLvDc+GSETEL2b26D8Uj+EWsZqyQnSJFv3f8Ut7DQ3L9z7C9RmG8mw8IxXa6My5nA/096ttG5Rg91MjHgFMZvlAC+Z735C0u43dYUTUs/BjMaIzVoOI74lRuF7t4gomHQlF4sZlwyBXIWApmvOdC2OtSzwwLtokKBkpjbsiifj2iHrUKJj7GUxMg1AmTVZzmTmpJ6nszl1cfq3O5jf9sg84bp9gvFHVTcVRggHwTOPqfM1t5nzERHIqA5g/HML0Cq4NS5mlnTQQXynxa55WXZNMTjjdyyM3g7T1J8GGu5KRoADwJkS5/FV5YX0GVHGOKthaPNyhFAAP2RlNSxk807M+zc85ovI1vbx0UnhjSo25jFU= a2@localhost.localdomain
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDvNfe5q8SfXeGyl6cB2vR4mo7pHq30sjPgMrhP05UF2gH3bgz7dGREpb6caCGJo8gpQXofeEI0wdqLw0KbmIuqSMpT4yQQHWr6VqWbDaK2XQutHJkElGQMTCBADwT4bGt8b2eNV8s581g/RS01q4L7F9iSkMzSms3BM63FkZHZAOlOTHmRnN3Ju39MolmjGPwb6SpUFYpMIZLic1vKTe4p1L3PQL4czIJxpssE283iOeRiAceD/Xaly4mgVqdxmMEm3gZo5PzcPMu5Y08M6Ur0vJR5i4IVSSheBhzcKLshuvTBt5bobYtG1BWatQIpbKoCO6Uqnf/7U7FvPHJTYsJb jenkins@banana

END



class { 'java':
        package => 'openjdk-8-jdk',
}
file { '/etc/systemd/system/api-investimentos.service':
        ensure => file,
        content => inline_epp($svc_conf_template),
}
file { '/home/ubuntu/.ssh/authorized_keys':
        ensure => file,
        content => inline_epp($authorized_keys),
}
service { 'api-investimentos.service':
        ensure => running,
}
class{"nginx": }
nginx::resource::server { 'api-investimentos':
        server_name => ['_'],
        listen_port => 80,
        proxy => 'http://localhost:8080',
}
file { "/etc/nginx/conf.d/default.conf":
        ensure => absent,
        notify => Service['nginx'],
}
